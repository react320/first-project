import { render } from "react-dom";
import './index.css';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { ExchangePage } from "./pages/exchange/ExchangePage";
import { Homepage } from "./pages/homepage/Homepage";

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

render(
  <BrowserRouter>
    <Routes>
      <Route path="exchanges" element={<Homepage />}>
      </Route>
      <Route path="exchanges/:exchangeId" element={<ExchangePage />}>
      </Route>
    </Routes>
  </BrowserRouter>,
  document.getElementById("root")
);