const COINPAPRIKA_BASEPATH = "https://api.coinpaprika.com/v1/"

export const getExchanges = () => {
return fetch(COINPAPRIKA_BASEPATH + "coins/doge-dogecoin/exchanges")
    .then(data => data.json())
}


export const getExchange = (id: any) => {
    return fetch(COINPAPRIKA_BASEPATH + "exchanges/" + id)
        .then(data => data.json())
}