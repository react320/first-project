import React, { useState, useEffect  } from 'react';
import { useParams } from 'react-router-dom';

import './ExchangePage.css';

import { getExchange } from '../../services/coinpaprika/exchanges';

type Exchange = {
    name: string,
    adjusted_volume_24h_share: string,
    description: string
}
export const ExchangePage = ()=>  {
    const [exchange, setExchange] = useState<Exchange>();
    let { exchangeId } = useParams();

    
    useEffect((): any => {
      getExchange(exchangeId).then(item => {
              setExchange(item)
      })
    }, [])

  return (
      <div className="Exchange">
        <h1 className="Exchange-name">{ exchange && exchange.name }</h1>
        <p className="Exchange-description">{ exchange && exchange.description }</p>
      </div>
  );
}


