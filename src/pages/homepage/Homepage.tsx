import React, { useState, useEffect } from 'react';

import './Homepage.css';
import { Card } from '../../components/card/Card';

import { getExchanges } from '../../services/coinpaprika/exchanges';
import { Link } from 'react-router-dom';

type Exchange = {
    id: string,
    name: string,
    adjusted_volume_24h_share: string
}
export const Homepage = ()=>  {
    const [exchanges, setExchanges] = useState<Exchange[]>([]);

    
    useEffect((): any => {
      getExchanges().then(items => {
              setExchanges(items)
      })
    }, [])

  return (
      <div>
        <h1>Exchanges</h1>
        <ul className="Exchanges-container">
            {
              exchanges.map((exchange): any => {
                return (
                    <Link to={exchange.id}>
                        <Card name={exchange.name} description={exchange.adjusted_volume_24h_share} />
                    </Link>
                    )
              })
              }
        </ul>
      </div>
  );
}


