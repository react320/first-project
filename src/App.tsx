import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Card } from './components/card/Card';
import { Homepage } from './pages/homepage/Homepage';



function App() {
  return (
    <Homepage />
  );
}

export default App;
