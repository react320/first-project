
import './Card.css';

type CardProps = {
    name: string,
    description: string
}
  
export const Card = (props: CardProps) => {
    return (
        <div className="Card">
            <h2 className="Card-title"> { props.name } </h2>
            <p className="Card-description"> {props.description} </p>
        </div>
      );
}